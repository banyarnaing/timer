//
//  TimerViewController.swift
//  TimerApp
//
//  Created by Banyar.
//  Copyright © 2018 bnh. All rights reserved.
//

import UIKit
import UserNotifications

class TimerViewController: UIViewController {
    @IBOutlet weak var timerLabel:UILabel!
    @IBOutlet weak var startPauseBtn:UIButton!
    @IBOutlet weak var stopBtn:UIButton!
    
    var timer:Timer?
    static var startingTime:Date?
    static var timerEndTime:TimeInterval = 0
    var remainingTime:TimeInterval?
    var sec:TimeInterval?
    private var timerMode = TimerMode.start
    
    private var taskHour: Int = 0
    private var taskMin: Int = 0
    private var taskSec: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let isPauseMode = UserDefaults.standard.value(forKey: PersistenceKey.isPauseMode) as? Bool, isPauseMode{
            self.remainingTime = UserDefaults.standard.value(forKey: PersistenceKey.remainTime) as? Double ?? 0
            self.timerLabel.text = secToMinSec(remainingTime ?? 0)
            self.timerMode = .pause
            self.pauseTimer()
        }
        else if let startTime = UserDefaults.standard.value(forKey: PersistenceKey.timerStartTime) as? Double{
            TimerViewController.timerEndTime = UserDefaults.standard.value(forKey: PersistenceKey.timerEndTime) as? Double ?? 0
            TimerViewController.startingTime = Date(timeIntervalSince1970: startTime)
            let currentTime = Date(timeIntervalSinceNow: 0)
            let differenceTime = currentTime.timeIntervalSince1970 - startTime
            updateSec(differenceTime)
        }
 
    }
    
    func setup(){
        UNNotiService.sharedInstance.authorize()
    }
    
    func updateSec(_ sec: TimeInterval){
        
            self.timerLabel.text = secToMinSec(sec)
            if sec > TimerViewController.timerEndTime {
                self.timerLabel.text = secToMinSec(0)
                self.timer?.invalidate()
            }else{
                self.sec = sec
                 countUP()
            }
    }
    
    
    func secToMinSec(_ sec:TimeInterval)->String{
        let hour = Int(sec / 3600)
        let min = Int((sec / 60).truncatingRemainder(dividingBy: 60))
        let rem = Int(sec.truncatingRemainder(dividingBy: 60))
        let hourStr = hour > 9 ? "\(hour)" : "0\(hour)"
        let minStr = min > 9 ? "\(min)" : "0\(min)"
        let remStr = rem > 9 ? "\(rem)" : "0\(rem)"
        return "\(hourStr) : \(minStr) : \(remStr)"
    }
    
    func convertToSecond(_ hour:Double, _ min:Double,_ sec:Double)->TimeInterval{
        
        return (hour * 3600) + (min * 60) + (sec)
    }
    
    func countUP(){
        
        self.timer =  Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (time) in
            if let startTime = TimerViewController.startingTime{
                let currentTime = Date(timeIntervalSinceNow: 0)
                let differenceTime = currentTime.timeIntervalSince1970 - startTime.timeIntervalSince1970
                if differenceTime > TimerViewController.timerEndTime{
                    self.timerLabel.text = self.secToMinSec(0)
                    self.timerMode = .start
                    self.startPauseBtn.setTitle("Start", for: .normal)
                    self.timer?.invalidate()
                }else{
                    self.remainingTime = TimerViewController.timerEndTime - differenceTime
                    self.timerLabel.text = self.secToMinSec(self.remainingTime ?? 0)
                }
                print(differenceTime)
                
            }
        })
        
    }
    
    @IBAction func startTimer(_ sender:UIButton){
        guard TimerViewController.timerEndTime > 0 || self.taskHour > 0 || self.taskMin > 0 || self.taskSec > 0  else {
            return
        }
        (UIApplication.shared.delegate as! AppDelegate).isNeedToStoreData = true
        if timerMode == .start {
            self.startTimer()
            self.timerMode = .pause
            (UIApplication.shared.delegate as! AppDelegate).isKilledWithPauseMode = false
        }else if timerMode == .pause{
            self.pauseTimer()
            self.timerMode = .resume
            (UIApplication.shared.delegate as! AppDelegate).isKilledWithPauseMode = true
        }else if timerMode == .resume{
            self.resumeTimer()
            self.timerMode = .pause
            (UIApplication.shared.delegate as! AppDelegate).isKilledWithPauseMode = false
        }
        
        
    }
    
    func startTimer(){
        self.startPauseBtn.setTitle("Pause", for: .normal)
        TimerViewController.timerEndTime = convertToSecond(Double(self.taskHour), Double(self.taskMin), Double(self.taskSec))
        TimerViewController.startingTime = Date(timeIntervalSinceNow: 0)
        if TimerViewController.timerEndTime > 0{
            UNNotiService.sharedInstance.timerUserNoti(with: TimerViewController.timerEndTime)
        }
        countUP()
        
    }
    
    func pauseTimer(){
        self.startPauseBtn.setTitle("Resume", for: .normal)
        (UIApplication.shared.delegate as! AppDelegate).remaintime = self.remainingTime ?? 0
        TimerViewController.startingTime = Date(timeIntervalSinceNow: 0)
        UNNotiService.sharedInstance.removeTimerNoti()
        self.timer?.invalidate()
    }
    
    func resumeTimer(){
        self.startPauseBtn.setTitle("Pause", for: .normal)
        TimerViewController.startingTime = Date(timeIntervalSinceNow: 0)
        TimerViewController.timerEndTime = self.remainingTime ?? 0
        if TimerViewController.timerEndTime > 0{
            UNNotiService.sharedInstance.timerUserNoti(with: TimerViewController.timerEndTime)
        }
        countUP()
    }
    
    @IBAction func resetTimer(_ sender:UIButton){
        self.timer?.invalidate()
        self.timerLabel.text = "\(self.taskHour.getStringValue()) : \(self.taskMin.getStringValue()) : \(self.taskSec.getStringValue())"
        self.timerMode = .start
        self.startPauseBtn.setTitle("Start", for: .normal)
        UNNotiService.sharedInstance.removeTimerNoti()
        (UIApplication.shared.delegate as! AppDelegate).isNeedToStoreData = false
        deleteAllPersistenceData()
    }
    
    @IBAction func createTask(_ sender:UIButton){
        let vc = AddTaskVC(nibName: "AddTaskVC", bundle: nil)
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    
}

extension TimerViewController: SelectTimeDelegate{
    func didSelectedHour(hour: Int) {
        self.taskHour = hour
        self.timerLabel.text = "\(self.taskHour.getStringValue()) : \(self.taskMin.getStringValue()) : \(self.taskSec.getStringValue())"
    }
    
    func didSelectedMin(min: Int) {
        self.taskMin = min
        self.timerLabel.text = "\(self.taskHour.getStringValue()) : \(self.taskMin.getStringValue()) : \(self.taskSec.getStringValue())"
    }
    
    func didSelectedSec(sec: Int) {
        self.taskSec = sec
        self.timerLabel.text = "\(self.taskHour.getStringValue()) : \(self.taskMin.getStringValue()) : \(self.taskSec.getStringValue())"
    }
    
    
}
