//
//  Constant.swift
//  TimerApp
//
//  Created by Banyar on 6/5/18.
//  Copyright © 2018 bnh. All rights reserved.
//

import Foundation

enum TimerMode {
    case start
    case pause
    case resume
}

struct PersistenceKey {
    static let timerStartTime = "timerstarttime"
    static let timerEndTime = "timerendtime"
    static let isPauseMode = "ispausemode"
    static let remainTime = "remaintime"
}

struct  UserNotificationId{
     static let taskDoneID = "usernotification.taskDone"
}
