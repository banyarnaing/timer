//
//  Extender.swift
//  TimerApp
//
//  Created by Banyar on 9/5/18.
//  Copyright © 2018 bnh. All rights reserved.
//

import Foundation

extension Int{
    func getStringValue() -> String {
        return self > 9 ?  "\(self)" : "0\(self)"
    }
}
