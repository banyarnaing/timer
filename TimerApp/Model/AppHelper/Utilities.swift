//
//  Utilities.swift
//  TimerApp
//
//  Created by Banyar on 6/5/18.
//  Copyright © 2018 bnh. All rights reserved.
//

import Foundation
import UIKit

func deleteAllPersistenceData(){
    UserDefaults.standard.removeObject(forKey: PersistenceKey.timerEndTime)
    UserDefaults.standard.removeObject(forKey: PersistenceKey.timerStartTime)
    UserDefaults.standard.removeObject(forKey: PersistenceKey.remainTime)
    UserDefaults.standard.removeObject(forKey: PersistenceKey.isPauseMode)
    UserDefaults.standard.synchronize()
}


