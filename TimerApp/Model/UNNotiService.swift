//
//  UNNotiService.swift
//  TimerApp
//
//  Created by Banyar on 6/5/18.
//  Copyright © 2018 bnh. All rights reserved.
//

import Foundation
import UserNotifications

class UNNotiService: NSObject{
    static let sharedInstance = UNNotiService()
    private let userNotiCenter = UNUserNotificationCenter.current()
    private override init() {}
    
    func authorize(){
        let options: UNAuthorizationOptions = [.alert, .sound , .badge]
        userNotiCenter.requestAuthorization(options: options) { (granted, error) in
            print(error ?? "No un auth error")
            guard granted else{
                print("User denied access")
                return
            }
            self.setup()
        }
    }
    
    func setup()  {
        self.userNotiCenter.delegate = self
    }
    
    func timerUserNoti(with interval:TimeInterval){
        let content = UNMutableNotificationContent()
        content.title = "Timer Finished"
        content.body = "Your timer is all done"
        content.sound = .default()
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: interval, repeats: false)
        let request = UNNotificationRequest(identifier: UserNotificationId.taskDoneID, content: content, trigger: trigger)
        
        userNotiCenter.add(request, withCompletionHandler: nil)
        
    }
    
    func removeTimerNoti() {
        userNotiCenter.removePendingNotificationRequests(withIdentifiers: [UserNotificationId.taskDoneID])
    }
    
}

extension UNNotiService : UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Did recieve response")
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Will present")
        let options: UNNotificationPresentationOptions = [.alert, .sound]
        completionHandler(options)
    }
}
