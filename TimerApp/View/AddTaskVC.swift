//
//  AddTaskVC.swift
//  TimerApp
//
//  Created by Banyar.
//  Copyright © 2018 bnh. All rights reserved.
//

import UIKit
protocol SelectTimeDelegate {
    func didSelectedHour(hour:Int)
    func didSelectedMin(min:Int)
    func didSelectedSec(sec:Int)
}

class AddTaskVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var pickerView:UIPickerView!
    @IBOutlet weak var doneBtn:UIButton!
    @IBOutlet weak var cancelBtn:UIButton!
    private var hours = [Int]()
    private var minutes = [Int]()
    private var seconds = [Int]()
    
    var delegate:SelectTimeDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup(){
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        for hour in 0...23{
            self.hours.append(hour)
        }
        
        for minSec in 0...59{
            self.minutes.append(minSec)
            self.seconds.append(minSec)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return hours.count
        case 1:
            return minutes.count
        case 2:
            return seconds.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return"\(hours[row]) hour"
        case 1:
            return "\(minutes[row]) min"
        case 2:
            return "\(seconds[row]) sec"
        default:
            return "0"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch component {
        case 0:
            self.delegate?.didSelectedHour(hour: hours[row])
        case 1:
            self.delegate?.didSelectedMin(min: minutes[row])
        case 2:
            self.delegate?.didSelectedSec(sec: seconds[row])
        default:
            print("Selected else")
        }
    }
    
    
    @IBAction func done(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender:UIButton){
        self.delegate?.didSelectedHour(hour: 0)
        self.delegate?.didSelectedMin(min: 0)
        self.delegate?.didSelectedSec(sec: 0)
        self.dismiss(animated: true, completion: nil)
    }
}
